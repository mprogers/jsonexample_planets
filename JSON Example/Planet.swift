//
//  Planet.swift
//  JSON Example
//
//  Created by Michael Rogers on 3/20/19.
//  Copyright © 2019 Michael Rogers. All rights reserved.
//

import Foundation

// Codable makes it possible to work with JSONEncoders and JSONDecoders
struct Planet : Codable {
    var name:String
    var mass:Double
    var distance:Double
}
